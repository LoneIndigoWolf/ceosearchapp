using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using NUnit.Framework;
using WebApplication;
using WebApplication.Models;

namespace Testing
{
    [TestFixture]
    public class SearchTestSpec
    {
        [SetUp]
        public void Init()
        {
            var webHost = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder().UseStartup<Startup>().Build();
        }

        [Test]
        //cant have more hits than results, cant have a negative amount of hits
        public async Task MakeSureSearchReturnsSaneResult()
        {
            //every result should have the search term in it so we should expect 12 results (definitely not more than 12)
            var result = await SearchService.Search(new SearchViewModel
                {SearchValue = "title search", TargetTerm = "title", NumberOfResultsToSearch = 12});
            
            Console.WriteLine("result: " + result);
            Assert.That(result, Is.EqualTo(12));
        }
        
        [Test]
        //set an unexpected endpoint
        public async Task EnsureSearchFailsGracefullyIfTaskFails()
        {
            var searchUrl = Startup.Configuration.GetSection("AppSettings")["ApiQueryUrl"] = "";
            
            Assert.DoesNotThrowAsync(async () => await SearchService.Search(new SearchViewModel
                {SearchValue = "infotrack", TargetTerm = "infotrack", NumberOfResultsToSearch = 20}));
        }
        
        [Test]
        //verify given two unrelated terms zero results are returned
        public async Task CheckZeroResults()
        {
            var result = await SearchService.Search(new SearchViewModel
                {SearchValue = "infotrack", TargetTerm = "puppies", NumberOfResultsToSearch = 100});
            
            Assert.That(result, Is.EqualTo(0));
        }
    }
}