using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class SearchViewModel
    {
        public string SearchValue { get; set; }
        public string TargetTerm { get; set; }
        public int NumberOfResultsToSearch { get; set; }
    }
}