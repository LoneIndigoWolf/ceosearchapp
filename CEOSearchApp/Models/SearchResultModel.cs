namespace WebApplication.Models
{
    public class SearchResultModel
    {
        public string SearchValue { get; set; }
        
        public int NumberOfResultsToSearch { get; set; }
        
        public int NumberOfHits { get; set; }

    }
}