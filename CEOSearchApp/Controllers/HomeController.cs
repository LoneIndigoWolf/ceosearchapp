﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(new SearchViewModel {SearchValue = "online title search", TargetTerm = "infotrack", NumberOfResultsToSearch = 100});
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SubmitQuery([Bind("SearchValue,NumberOfResultsToSearch,TargetTerm")] SearchViewModel searchRequest)
        {
            if (ModelState.IsValid)
            {
                var searchResult = new SearchResultModel();
                searchResult.NumberOfHits = await SearchService.Search(searchRequest);
                return View(searchResult);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}