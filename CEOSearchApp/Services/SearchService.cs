using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace WebApplication.Models
{
    public static class SearchService
    {

        private static List<int> GetToplevelDivPositionsInString(string resultString)
        {
            var lastIndexedDivPosition = 0;
            var topLevelDivPositions = new List<int>();
            var currentLevel = 1;
            while (lastIndexedDivPosition + 1 < resultString.Length)
            {
               
                var nextOpenDivPosition = resultString.IndexOf("<div", lastIndexedDivPosition + 1, StringComparison.Ordinal);
                var nextClosedDivPosition = resultString.IndexOf("</div>", lastIndexedDivPosition + 1, StringComparison.Ordinal);
                
                //if we havent found a close div, just assume the end of the string (fail gracefully) - if we cant find a start div, there are no more top levels
                if(nextOpenDivPosition == -1)
                    break;
                
                if (nextClosedDivPosition == -1)
                    nextClosedDivPosition = resultString.Length;

                if (nextOpenDivPosition < nextClosedDivPosition)
                {
                    if(currentLevel == 1)
                        topLevelDivPositions.Add(nextOpenDivPosition);
                    
                    lastIndexedDivPosition = nextOpenDivPosition;
                    currentLevel++;
                }
                else
                {
                    lastIndexedDivPosition = nextClosedDivPosition;
                    currentLevel--;
                }
            }

            topLevelDivPositions.Sort();
            
            //another gross component which could be solved by using the proper API - the top three and bottom 4 div positions need to be merged as they are false divs
            topLevelDivPositions.RemoveRange(topLevelDivPositions.Count - 4, 3);
            topLevelDivPositions.RemoveRange(0, 3);
            
            topLevelDivPositions.Insert(0, 0);

            return topLevelDivPositions;
        }

        private class SearchResultItem
        {
            public int Index = 0;
            public bool WasAHit = false;
        }

        private class SearchResultItems : List<SearchResultItem>
        {
            public int PageNumber = 0;
            public string Error = null;
        }
        private static async Task<SearchResultItems> RunSearch(string query, int searchStartPoint, string valueToFind)
        {
            var taskResponse = new SearchResultItems{PageNumber = searchStartPoint};
            HttpClient http = new HttpClient();
            var response = new byte[0];
            try
            {
                response = await http.GetByteArrayAsync(query + "&start=" + searchStartPoint);
            }
            catch (Exception)
            {
                taskResponse.Error = "Could not reach the search service. Is google down?";
                return taskResponse;
            }
            
            String source = Encoding.GetEncoding("utf-8").GetString(response, 0, response.Length - 1);
            source = WebUtility.HtmlDecode(source);
            source = source.Substring(source.IndexOf("<div id=\"main\"") + 1);
            //seems like each search 'result' is encapsulated in a top level div inside the body.
            //encapsulate these together to insure that we dont get multiple 'hits' for the same result.
            //Assuming a human doing this counting would count one result as a 'hit' no matter how many times the term was found inside that result
            
            //it seems like the accepted solution online is to use the HTML agility pack so if there wasnt a requirement to not use 3rd party libraries this is probably a time i would

            //so, we have to find the start indexes for all the top level divs. This is gross, fragile, and not perfect in any way (definitely correct to use a 3rd party library here)

            var divPositions = GetToplevelDivPositionsInString(source);
            
            var searchResultIndex = 0;
            while (divPositions.Count > 0)
            {
                var currentDivPosition = divPositions[0];
                var nextDivPosition = source.Length;
                divPositions.RemoveAt(0);
                if (divPositions.Count > 0)
                {
                    nextDivPosition = divPositions[0];
                }

                taskResponse.Add(new SearchResultItem
                {
                    WasAHit = source.Substring(currentDivPosition, nextDivPosition - currentDivPosition)
                        .Contains(valueToFind),
                    Index = searchResultIndex
                });

            }

            return taskResponse;
        }
        
        public static async Task<int> Search(SearchViewModel searchRequest)
        {
            HttpClient http = new HttpClient();
            var searchUrl = Startup.Configuration.GetSection("AppSettings").GetValue<string>("ApiQueryUrl") + searchRequest.SearchValue + "&num=" + Startup.Configuration.GetSection("AppSettings").GetValue<int>("ApiPageSize");
            var searchTasks = new List<Task<SearchResultItems>>();

            var hitsFound = 0;

            //google search seems to return AT LEAST 10 'results' for every page (sometimes more if there are ads). Except the first page which seems to return 8. so we can work out what URLs we need to hit upfront and hit them all asynchronously
            //if we find zero result divs, either there are none, or we have hit a rate limit/captcha, so give up
            var searchStartPoint = 0;
            while (searchStartPoint < searchRequest.NumberOfResultsToSearch)
            {
                searchTasks.Add(RunSearch(searchUrl, searchStartPoint, searchRequest.TargetTerm));
                searchStartPoint += Startup.Configuration.GetSection("AppSettings").GetValue<int>("ApiPageSize");
            }
            
            await Task.WhenAll(searchTasks);

            var searchResultCount = 0;
            
            foreach (var searchTask in searchTasks.OrderBy(x => x.Result.PageNumber))
            {
                var result = await searchTask;
                foreach (var searchResultItem in result)
                {
                    if (searchResultCount < searchRequest.NumberOfResultsToSearch && searchResultItem.WasAHit)
                    {
                        hitsFound++;
                    }

                    searchResultCount++;
                }
            }

            return hitsFound;
        }
    }
}